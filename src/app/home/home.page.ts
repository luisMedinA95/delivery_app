import { Component, OnInit } from '@angular/core';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapsAnimation, MyLocation, GoogleMapsMapTypeId} from "@ionic-native/google-maps";
import { Platform, ToastController, LoadingController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  map: GoogleMap;
  loading: any;
  constructor(private platform: Platform, public loadingCtrl: LoadingController, public toastCtrl: ToastController, private menu: MenuController) { }
  ngOnInit() {
    this.menu.enable(false);
  }

  Loa(){
     // Creamos un componente de Ionic para mostrar un mensaje
    // mientras obtenemos esperamos que termine el proceso de
    // obtener la ubicación
    this.loading = this.loadingCtrl.create({
      message: "Espera por favor..."
    });

    // Presentamos el componente creado en el paso anterior
    this.loading.present();

    this.loading.dismiss();
  }
}


